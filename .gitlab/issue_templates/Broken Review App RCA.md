## Context

We see the following errors in some review-apps jobs (e.g. <JOB_LINK>):

```shell
```

## Goal

Find out the root cause of the problem.

/label ~"Engineering Productivity" ~"review-apps-broken" ~"type::bug" ~"bug::availability"
